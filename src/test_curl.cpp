//============================================================================
// Name        : test_curl.cpp
// Author      : Rafal
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#ifdef __unix__
#elif defined(_WIN32) || defined(WIN32)
#define OS_Windows
#endif

#include <iostream>
#include <sstream>
#include <fstream>
#include <streambuf>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include <curl/curl.h>
#include <sys/stat.h>
#include "MyTime.hpp"

using namespace std;

Tools::MyTime tim;
/**
 *  Write data function/event called when file saved on disk
 */
static size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream)
{
  size_t written = fwrite(ptr, size, nmemb, (FILE *)stream);
  if(tim.secondPass()) {
	  cout<<"."<<std::flush;
  }
  return written;
}

/**
 * WriteCallback reads filelist
 */
static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

/**
 * Create all subdirestories
 */
bool mkpath( std::string path )
{
    bool bSuccess = false;
	#ifdef OS_Windows
    /* Windows code */
    int nRC = ::mkdir( path.c_str());
    #else
    /* GNU/Linux code */
    int nRC = ::mkdir( path.c_str(), 0775 );
    #endif

    if( nRC == -1 )
    {
        switch( errno )
        {
            case ENOENT:
                //parent didn't exist, try to create it
                if( mkpath( path.substr(0, path.find_last_of('/')) ) )
                    //Now, try to create again.

					#ifdef OS_Windows
					/* Windows code */
                	 bSuccess = 0 == ::mkdir( path.c_str());
					#else
					/* GNU/Linux code */
                	 bSuccess = 0 == ::mkdir( path.c_str(), 0775 );
					#endif

                else
                    bSuccess = false;
                break;
            case EEXIST:
                //Done!
                bSuccess = true;
                break;
            default:
                bSuccess = false;
                break;
        }
    }
    else
        bSuccess = true;
    return bSuccess;
}

std::string downloadFile(string url, string file) {
	  CURL *curlh;
	  std::string out;

	  string urlfile = url+file; //"list.game";

	  curlh = curl_easy_init();
	  curl_easy_setopt(curlh, CURLOPT_URL,urlfile.c_str());
	  curl_easy_setopt(curlh, CURLOPT_VERBOSE, 0L);
	  curl_easy_setopt(curlh, CURLOPT_NOPROGRESS, 1L);

	  /* send all data to this function  */
	  curl_easy_setopt(curlh, CURLOPT_WRITEFUNCTION, WriteCallback);
	  curl_easy_setopt(curlh, CURLOPT_FAILONERROR, true);
	  curl_easy_setopt(curlh, CURLOPT_WRITEDATA, &out);

	  CURLcode res = curl_easy_perform(curlh);
	 /* Check for errors */
	 if (res != CURLE_OK) {
		 fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
	 }

	  /* cleanup curl stuff */
	 curl_easy_cleanup(curlh);
	 curl_global_cleanup();

	 return out;
}

void downloadFileLocal(string url, string file ) {
	  tim.reset();
	  string fullurl = url+file;

	  std::size_t found = file.find(".game");
	  if (found!=std::string::npos) {
		  return;
	  	  }

	  double ct;
	  FILE *pagefile;
	  CURL *curl_handle = curl_easy_init();

	  curl_easy_setopt(curl_handle, CURLOPT_URL, fullurl.c_str());
	  curl_easy_setopt(curl_handle, CURLOPT_VERBOSE, 0L);
	  curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 1L);
	  curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, write_data);

	 //  progress event
	 //  int progress_callback(void *clientp,double dltotal,double dlnow,double ultotal,double ulnow);
     //  CURLcode curl_easy_setopt(CURL *handle, CURLOPT_PROGRESSFUNCTION, progress_callback);

	  std::cout << "File: " << file;

	  string directory;
	  const size_t last_slash_idx = file.rfind('//');
	  if (std::string::npos != last_slash_idx)
	  {
	      directory = file.substr(0, last_slash_idx);
	      mkpath(directory);
	  }

	  pagefile = fopen(file.c_str(), "wb");
	  if(pagefile) {
		 /* write the page body to this file handle */
		 curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, pagefile);
		 CURLcode res = curl_easy_perform(curl_handle);
		 /* Check for errors */
		 if (res != CURLE_OK) {
			 fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
		 } else {
		    res = curl_easy_getinfo(curl_handle, CURLINFO_CONTENT_LENGTH_DOWNLOAD, &ct);
			// cout << " .. " << std::setprecision(3) << <<" MB"<< std::endl;
			printf(".. %4.3f MB \n",(ct/1048576));
		 }

		 fclose(pagefile);
	  }

	  /* cleanup curl stuff */
	  curl_easy_cleanup(curl_handle);
	  curl_global_cleanup();
}

string readVersion() {
	std::ifstream t("version.ini");
	std::stringstream buffer;
	buffer << t.rdbuf();

	return buffer.str();
}

/**
 * ==================================
 * ============== MAIN ==============
 * ==================================
 */
int main(int argc, char *argv[])
{
  cout<<"--[ Game Client Downloader v0.1  ]--"<<endl;
  cout<<"--[ 2019 (C) reCone.org          ]--"<<endl;

  string url("http://download.recone.org/3d/");

  curl_global_init(CURL_GLOBAL_ALL);

  // read localversion
  std::string localversion = readVersion();
  // download version
  std::string version = downloadFile(url,"version.ini");

  if(localversion.compare(version) == 0) {
	  cout<<"Nothing to download!"<<endl;
	  return 0;
  }

  // download file list
  std::string buffor = downloadFile(url,"list.game");
  std::istringstream stream(buffor);

  std::string line;
  while (std::getline(stream, line)) {
    downloadFileLocal(url,line);
  }

  cout<<"All files downloaded! Done."<<endl;
  return 0;
}
