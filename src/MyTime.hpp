/*
 * Time.h
 */

#ifndef MyTIME_H_
#define MyTIME_H_

#include <math.h>
#include <sys/time.h>
#include <cstdlib>
#include <string>

using namespace std;

namespace Tools {
  class MyTime {
	private:
		double lastSec;   // time passed
		struct timespec start, end;

	public:
		/*
		 * Start it before using  object
		 */
		MyTime();

		/*
		 * Reset start time
		 */
		void reset();

		double upTimeMiliseconds();
		/*
		 * How meny seconds was spend from startTimer
		 */
		double upTimeSeconds();

		/*
		 * Checks whether the 1s time was spend
		 */
		bool secondPass();
		/*
		 * Get return actual time as a text, redable
		 */
		const string getAcTime();

		time_t getAcTimeStamp();

		/*
		 * Return GMT Time
		 */
		const string getGMTime(time_t t);

	};
}

#endif /* MyTIME_H_ */
