/*
 * Time.cpp
 *
 *  Created on: 9 Jan 2014
 *      Author: rafles01
 */

#include "MyTime.hpp"
namespace Tools {
	/*
	 * Start it before using  object
	 */
	MyTime::MyTime() {
		lastSec = 0;
		clock_gettime(CLOCK_MONOTONIC, &start);
	}

	/*
	 * Reset start time
	 */
	void MyTime::reset() {
		clock_gettime(CLOCK_MONOTONIC, &start);
	}

	double MyTime::upTimeMiliseconds() {
		clock_gettime(CLOCK_MONOTONIC, &end);

		return ((end.tv_nsec - start.tv_nsec) / 1e6 + (end.tv_sec - start.tv_sec) * 1e3);
	}

	/*
	 * How meny seconds was spend from startTimer
	 */
	double MyTime::upTimeSeconds() {
		return upTimeMiliseconds() / 1000;
	}

	/*
	 * Checks whether the 1s time was spend
	 */
	bool MyTime::secondPass() {
		double actSec = 0;
		actSec = floor(upTimeSeconds());

		if(actSec != lastSec) {
			lastSec = actSec;
			return true;
		}

		return false;
	}

	/*
	 * Get return actual time as a text, redable
	 */
	const string MyTime::getAcTime() {
		time_t actualtime = 0;
		time(&actualtime);

		string buf(ctime(&actualtime));
		buf.replace(buf.length()-1,1,"");

		return buf;
	}

	time_t MyTime::getAcTimeStamp() {
		time_t actualtime = 0;
		return time(&actualtime);
	}

	/*
	 * Return GMT Time
	 */
	const string MyTime::getGMTime(time_t t) {
		char buffer[250];
		strftime(buffer,250,"%a, %d %b %Y %X GMT",localtime(&t));

		return buffer;
	}
}
